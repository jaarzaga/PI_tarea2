/**********************************************************************
		Header that includes all the functions 
		structures to be used.
**********************************************************************/
/*Include Libraries*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*Constats*/
#define PIXEL_MAX_VAL 	255
#define PIXEL_MIN_VAL 	0

#define PI 		3.141592654

#define PASS_HIGH	1
#define PASS_LOW	-1

#define F_IDEAL		1
#define F_BUTTERWORTH	2
#define F_GAUSSIAN	3

/* Read the contents of the input image file in PGM format*/
int read_pgm_image(char *infilename, unsigned char **image, int *rows, int *cols);

/* Write the output image file in PGM format */
int write_pgm_image(char *outfilename, unsigned char *image, int rows,
    int cols, char *comment, int maxval);

/* Normalize a float matriz to 0-255 in order to display it as image, writes it as pgm image*/
int dftspecter(float *dft, int size, char *filename);

/* Calculate 2D DFT of a given input float array, trough FFT*/
int DFT(float **image, int size, float dir);

/*Compute 1D FFT of a given input data*/
fft(float data[], int num, float dir);

/*Receive two arrays, depending of direction multiplies one of them for (-1)^(x+y) and stores it on the other*/
preprocesing(unsigned char **image, float **dft,int rows, int cols, int size, float dir);

/*Allocate memory for two arrays, one of them will be a square of a power of 2*/
int memoryallocation(int cols, int rows, unsigned char **image, float **dft);

/*Generate a filter on frquency spectrum*/
generate_filter(float **h, int size, int fc, int type, int filter, int order);

/*Multiply two arrays of data, element by element*/
apply_filter(float **dft, float *h, int size);

/* End of File */
