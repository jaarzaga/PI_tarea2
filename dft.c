/**********************************************************************
				
				 dft.c

	 This file contains functions to perform discrete fourier 
	 transform, both forward and inverse
	
	 -dft: function takes an input image and return two 2D arrays 
	 (real and imaginary component) of a discrete fourier transform

	 -idft: function takes two 2D arrays (real and imaginary
	 components) and return its inverse discrete fourier trasnform

**********************************************************************/
/*Include Libraries*/
#include "Functions.h"

/* Normalize a float matriz to 0-255 in order to display it as image, writes it as pgm image*/
int dftspecter(float *dft, int size, char *filename){
	int i, j, pixel;
	float max, min, norm, mag[size*size], magnitude,value, offset=0;
	unsigned char img[size*size];

	/*Calculate magnitud of input array by (r*r+i*i)^(1/2)
	Finds the max and min value from the resulting array*/
	for(i=0;i<size;i++){
		for(j=0;j<size;j++){
			pixel=i*size*2+j*2;
			mag[i*size+j]=sqrt(dft[pixel]*dft[pixel]+dft[pixel+1]*dft[pixel+1]);
			if(i==0 && j==0){ max = min = mag[i*size+j]; }
			if(mag[i*size+j]>max){ max=mag[i*size+j]; }
			if(mag[i*size+j]<min){ min=mag[i*size+j]; }
		}
	}

	/*Applies an offset to the data array in order to work only with positive values*/
	if (min<=0){
		offset=1-min;
		min=min+offset;
		max=max+offset;

	}

	/*Aplies log to each value of array*/
	min=log10(min);
	max=log10(max);
	for(i=0;i<size;i++){
		for(j=0;j<size;j++){
 			mag[i*size+j]=log10(mag[i*size+j]+offset);
		}
	}

	/*Get factor to normalize between 0 and 255*/
	norm = PIXEL_MAX_VAL/(max-min);

	/*Normalize each value by a norm factor and writ it to output array*/
   	for(i=0;i<size;i++){
		for(j=0;j<size;j++){
 			value=(mag[i*size+j]-min) * norm;
			if (value > PIXEL_MAX_VAL){
				img[i*size+j]=PIXEL_MAX_VAL;
			} else if (value < PIXEL_MIN_VAL){
				img[i*size+j]=PIXEL_MIN_VAL;
			} else {
				img[i*size+j]=(unsigned char)value;
			}
		}
	}

	/*Write output image*/
	write_pgm_image(filename, img, size, size, NULL, PIXEL_MAX_VAL);
    	return(1);
}	

/* Calculate 2D DFT of a given input float array, trough FFT*/
int DFT(float **image,int size, float dir){
	int X, Y, num;
	long int R;
	float data[size*2], scale;
	
	/*Gets facto to sacle depending on direction*/
	num=size;
	if (dir== 1.0)
		scale=num*num;
	else if (dir==-1.0)
		scale=1.0;
	else 
		return 0;

	/*Applies FFT on each rows*/
	for(Y=0;Y<num;Y++){
		R=(long)Y*size*2;
		for(X=0;X<=2*num-1;X++){
			data[X]=(*image)[R+X];
		}
		fft(data, num, dir);
		for(X=0;X<=2*num-1;X++){
			(*image)[R+X]=data[X];
		}
	}

	/*Apllies FFT on each column*/
	for(X=0;X<=2*num-1;X+=2){
		for(Y=0;Y<num;Y++){
			R=(long)Y*size*2;
			data[2*Y]=(*image)[R+X];
			data[2*Y+1]=(*image)[R+X+1];
		}
		fft(data, num, dir);
		for(Y=0;Y<num;Y++){
			R=(long)Y*size*2;
			(*image)[R+X]=data[2*Y]/scale;
			(*image)[R+X+1]=data[2*Y+1]/scale;
		}
	}
	return 1;
}

/*Compute 1D FFT of a given input data*/
fft(float data[], int num, float dir){
	int array_size, bits, ind, j, j1;
	int i, i1, u, step, inc;
	float sine[num+1], cose[num+1];
	float wcos, wsin, tr, ti, temp;
	
	bits=log(num)/log(2)+.5;
	for(i=0;i<num+1;i++){
		sine[i]=dir*sin(PI*i/num);
		cose[i]=cos(PI*i/num);
	}
	
	/*Reorder the input data*/
	array_size=num<<1;
	for(i=0;i<num;i++){
		ind=0;
		for(j=0;j<bits;j++){
			u=1<<j;
			ind=(ind<<1)+((u&i)>>j);		
		}
		ind=ind<<1;
		j=i<<1;
		if(j<ind){
			temp=data[j];
			data[j]=data[ind];
			data[ind]=temp;
			temp=data[j+1];
			data[j+1]=data[ind+1];
			data[ind+1]=temp;
		}
	}	
	
	/*Aplies FFT on reordered data*/
	for(inc=2;inc<array_size;inc=step){
		step=inc<<1;
		for(u=0;u<inc;u+=2){
			ind=((long)u<<bits)/inc;
			wcos=cose[ind];
			wsin=sine[ind];
			for(i=u;i<array_size;i+=step){
				j=i+inc;
				j1=j+1;
				i1=i+1;
				tr=wcos*data[j]-wsin*data[j1];
				ti=wcos*data[j1]+wsin*data[j];
				data[j]=data[i]-tr;
				data[i]=data[i]+tr;
				data[j1]=data[i1]-ti;
				data[i1]=data[i1]+ti;
			}
		}
	}
}

/*Allocates memory and initialize it to 0, adjust size of dft array to be 
a square of (n^2)*(n^2)*/
int memoryallocation(int cols, int rows, unsigned char **image, float **dft){
	int n,size;
	
	/*Gets the power of 2, equal or closer upwards to dimensions of input array*/
	if(cols>rows){ size=cols; }
	else { size=rows; }
	n=0;
	while((int)size/2>0){
		if(size%2){size++;}
		size/=2;	
		n++;
	}
	for(size=1;n>0;size*=2,n--);

	/*Allocate memory for output image, equal to input image*/
	if(((*image) = calloc((rows*cols),sizeof (unsigned char))) == NULL){
		printf("Memory allocation failure.\n");
		return(0);
	}

	/*Allocate memory for DFT, as square of (2^n)*(2^n)*/
	if(((*dft) = calloc((2*size*size),sizeof (float))) == NULL){
		printf("Memory allocation failure.\n");
		return(0);
	}

	return size;
}

/*Receive two arrays, depending of direction multiplies one of them for (-1)^(x+y) and stores it on the other*/
preprocesing(unsigned char **image, float **dft, int rows, int cols, int size, float dir){
	int i, j;	 
	double value;

	/*When dir equal to 1, multiply char data by (-1)^(x+y), store it as float on DFT*/
	if(dir == 1){
		for(i=0;i<rows;i++){
			for(j=0;j<cols;j++){
				(*dft)[i*size*2+j*2]=(*image)[i*cols+j]*(float)pow(-1,i+j);
			}
		}
	/*When dir equal to -1, multiply float on DFT by (-1)^(x+y), store it as char on image*/
	/*Also verify min and max values to match 0 and 255*/
	} else if (dir == -1){	
		for(i=0;i<rows;i++){
			for(j=0;j<cols;j++){
				value=(double)(*dft)[i*size*2+j*2]*pow(-1,i+j);
				if (value > PIXEL_MAX_VAL){
					(*image)[i*cols+j]=PIXEL_MAX_VAL;
				} else if (value < PIXEL_MIN_VAL){
					(*image)[i*cols+j]=PIXEL_MIN_VAL;
				} else {
					(*image)[i*cols+j]=(unsigned char)(value);
				}
			}
		}
	}
}

/*Generate a filter on frquency spectrum*/
generate_filter(float **h, int size, int fc, int type, int filter, int order){
	int i, j, pixel;
	float huv, d, div, pot, sum, div2,expo;

	if(((*h) = calloc((2*size*size),sizeof (float))) == NULL){
		printf("Memory allocation failure.\n");
		return(0);
	}

	/*Fills filter with data point by point*/
	for(i=0;i<size;i++){
		for(j=0;j<size;j++){
			/*Get distance of current point to center of dft array*/
			pixel=i*size*2+j*2;
			d=sqrt((i-size/2)*(i-size/2)+(j-size/2)*(j-size/2));
			/*Apply the formula of corresponding filter*/
			if(filter == F_IDEAL){
				if(type == PASS_LOW){
					if(d<=fc){ huv=1; } 
					else { huv=0; }
				} else if(type == PASS_HIGH){
					if(d<=fc){ huv=0;} 
					else { huv=1;}
				}
			}else if (filter == F_BUTTERWORTH){
				if(type == PASS_LOW){ huv = 1/(1+pow((d/fc),2*order)); }
				else { huv = 1/(1+pow((fc/(d+0.0000001)),2*order)); }
			}else if (filter == F_GAUSSIAN){
				if(type == PASS_LOW){ huv = exp((-1*d*d)/(2*fc*fc)); }
				else { huv = 1-exp((-1*d*d)/(2*fc*fc)); }
			}else { printf("Error"); }
			/*Store value on real and imaginary component of dft*/
			(*h)[pixel]=huv;
			(*h)[pixel+1]=huv;
		}
	}
}

/*Multiply two arrays of data, element by element*/
apply_filter(float **dft, float *h, int size){
	int i,j, pixel;
	
	/*Multiply first and second array, stores result on dft*/
	for(i=0;i<size;i++){
		for(j=0;j<size*2;j++){
			pixel=i*size*2+j;
			(*dft)[pixel]*=h[pixel];
		}
	}
}



/* End of File */
