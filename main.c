/**********************************************************************
				
				 Tarea1.c	
	 Uses functinos of image_io.c to read a PGM file and save its 
	 content in a diferent file .

**********************************************************************/
/*Include Libraries*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Functions.h"

int  main(int argc, char *argv[]){ 
	/*Declare variables to be used*/
	int cols=0, rows=0, size, result, opt, order,filter,type,D0;
	unsigned char *imagein,*imageout;
	char filename[70], filename2[70], sfilter[15], sorder[15]={0}, sD0[15]={0};
	float *dft, *h;
	FILE *fp;
	
	/*Ask for name of file to be procesed*/
	printf("Start of program\n");
	printf("Type (without .pgm) the name of the file to open (+ default lenna.pgm): ");
	scanf("%s", filename);

	/*Displays menua of available filters*/
	printf("************************************************\n");
	printf("************************************************\n");
	printf("**  Select the filter to apply                **\n");
	printf("**  LOW PASS                                  **\n");
	printf("**    1) Ideal                                **\n");
	printf("**    2) Butterworth                          **\n");
	printf("**    3) Gaussian                             **\n");
	printf("**                                            **\n");
	printf("**  HIGH PASS                                 **\n");
	printf("**    4) Ideal                                **\n");
	printf("**    5) Butterworth                          **\n");
	printf("**    6) Gaussian                             **\n");
	printf("**                                            **\n");
	printf("************************************************\n");
	printf("************************************************\n");

	/*Ask for D0, as cut frequency and number of filter to be used*/
	printf("\nType the value of D0: ");
	scanf("%d", &D0);
	printf("\nType the number of the filter to apply: ");
	if(scanf("%d", &opt)>0){
		/*Review if input filter is valid, in base of selection defines
			filter: could be Ideal, Butterworth, Gaussian
			type: could be low pass or high pass
			order: given by user for butterworth, 0 for any other
		*/
		switch(opt){
			case 1:
				type=PASS_LOW;
				filter=F_IDEAL;
				order=0;
				strcpy(sfilter,"_ILPF");
				break;
			case 2: 
				type=PASS_LOW;
				filter=F_BUTTERWORTH;
				printf("\nType the desired order of the filter: ");
				scanf("%d", &order);
				strcpy(sfilter,"_BLPF");
				break;
			case 3:
				type=PASS_LOW;
				filter=F_GAUSSIAN;
				order=0;
				strcpy(sfilter,"_GLPF");
				break;
			case 4:
				type=PASS_HIGH;
				filter=F_IDEAL;
				order=0;
				strcpy(sfilter,"_IHPF");
				break;
			case 5: 
				type=PASS_HIGH;
				filter=F_BUTTERWORTH;
				printf("\nType the desired order of the filter: ");
				scanf("%d", &order);
				strcpy(sfilter,"_BHPF");
				break;
			case 6:
				type=PASS_HIGH;
				filter=F_GAUSSIAN;
				order=0;
				strcpy(sfilter,"_GHPF");
				break;
			default:
				printf("Invalid option\n\n");
				return 0;
		} 
	}else {
		printf("Invalid option");
		return 0;
	}

	/*Verify if defaul name was selected*/ 
	if(strncmp(filename,"+",1)==0){strcpy(filename, "lenna");}

	/*Read given image file*/
	strcpy(filename2, filename);
	strcat(filename2,".pgm");
	if (read_pgm_image(filename2, &imagein, &rows, &cols) == 0){
		return 0;
	}

	/*Generates base file name for output files InputFile_Filter_D0xxx_Oyyy_zzz*/
	snprintf(sD0, sizeof(sD0), "_D%d", D0);
	snprintf(sorder, sizeof(sorder), "_O%d", order);
	strcat(filename,sfilter);
	strcat(filename,sD0);
	strcat(filename,sorder);

	/*Allocates memory for DFT and procesed image, and initialize it to 0, 
	Allocated memory is adjust to be an array of (2^n)*(2^n) 
	even if input image is not square*/
	size = memoryallocation(cols, rows, &imageout, &dft);
	strcpy(filename2, filename);

	/*Pre process the image multipling for -1^(x+y) and stores it as float in dft*/
	preprocesing(&imagein, &dft,rows,cols,size,1);
	
	/*Calculate DFT, write an image with normalized magnitude of DFT as image*/	
	if (DFT(&dft, size, 1) == 0){return 0;}
	strcpy(filename2, filename);
	strcat(filename2,"_dft1.pgm");
	dftspecter(dft,size,filename2);

	/*Generates filter on frequency spectrum with given characteristicas, write 
an image with normalized magnitude of filter as image*/
	generate_filter(&h,size,D0,type,filter,order);
	strcpy(filename2, filename);
	strcat(filename2,"_h.pgm");
	dftspecter(h,size,filename2);

	/*Applies filters to image, write an image with normalized magnitud of 
	DFT as image*/	
	apply_filter(&dft, h, size);
	strcpy(filename2, filename);
	strcat(filename2,"_dft2.pgm");
	dftspecter(dft,size,filename2);

	/*Calculate IDFT*/	
	if (DFT(&dft, size, -1) == 0){return 0;}
	
	/*Post process the image multipling for -1^(x+y) and stores it as char in imageout*/
	preprocesing(&imageout, &dft,rows,cols,size,-1);
	printf("Succesfully processed\n");
		
	/*Writes output processed image file*/
	strcpy(filename2, filename);
	strcat(filename2,"_out.pgm");
	if(write_pgm_image(filename2, imageout, rows, cols,NULL, PIXEL_MAX_VAL)==0){
		printf("Error writting the output file.\n\n");
	} else{
		printf("Succesfully writed\n\n");
	}

	/*Release memory allocated for images, DFT and filter*/
	free(imagein);
	free(imageout);
	free(h);
	free(dft);
	
    	return 1;
}


